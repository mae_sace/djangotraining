from django.contrib import admin

from .models import Question, Choice

# Register your models here.
class InlineChoice(admin.TabularInline):
    model = Choice
    extra = 3

class QuestionModel(admin.ModelAdmin):
    model = Question
    inlines = [InlineChoice]
    search_fields = ['question_text']
    list_filter = ['pub_date']
    list_display = ('question_text', 'was_published_recently')
    fieldsets = (
        (None, {'fields' : ['question_text']}),
        ('Published date', {'fields' : ['pub_date']}),
    )

admin.site.register(Question,QuestionModel)