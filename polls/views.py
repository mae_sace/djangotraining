from django.shortcuts import render, reverse, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic

from .models import Question,Choice

class IndexView(generic.ListView):
    model = Question
    template_name = 'polls/index.html'

    def get_context_data(self):
        latest_questions = Question.objects.order_by('-pub_date')[:5]
        return {'questions' : latest_questions}

class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

class ResultsView(generic.DetailView):
    template_name = 'polls/results.html'
    model = Question
    
def vote(request,question_id):
    selected_choice = request.POST['choice']
    choice = Choice.objects.get(pk=selected_choice)
    choice.votes += 1
    choice.save()
    return HttpResponseRedirect(reverse('polls:results', args=[question_id]))