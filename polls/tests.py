from django.test import TestCase
from .models import Question, Choice
from django.utils import timezone

import datetime

# Create your tests here.
class QuestionTestCase(TestCase):

    def test_questions_created_recently_with_future(self):
        future = timezone.now() + datetime.timedelta(days=2)
        question = Question.objects.create(question_text="Test", pub_date=future)
        self.assertFalse(question.was_published_recently())

    
    def test_questions_created_recently_with_today(self):
        now = timezone.now()
        question = Question.objects.create(question_text="Test", pub_date=now)
        self.assertTrue(question.was_published_recently())


    def test_questions_created_recently_with_yesterday(self):
        yesterday = timezone.now() - datetime.timedelta(days=1)
        question = Question.objects.create(question_text="Test", pub_date=yesterday)
        self.assertTrue(question.was_published_recently())

    def test_questions_created_recently_with_past(self):
        past = timezone.now() - datetime.timedelta(days=5)
        question = Question.objects.create(question_text="Test", pub_date=past)
        self.assertFalse(question.was_published_recently())