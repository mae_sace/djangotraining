from django.db import models
from django.utils import timezone

import datetime

class Question(models.Model):
    question_text = models.CharField(max_length=50)
    pub_date = models.DateTimeField('date published', default=datetime.datetime.now)

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        yesterday = now - datetime.timedelta(days=1)
        return yesterday <= self.pub_date <= now

    was_published_recently.boolean = True

class Choice(models.Model):
    question = models.ForeignKey('Question',on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=50)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
